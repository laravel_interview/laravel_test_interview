@extends('products.layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>List of Products</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('products.create') }}"> Create New Product</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>SKU</th>
            <th>Name</th>
            <th>Price</th>
            <th>Category</th>
            <th>QTY</th>
            <th>Image</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($products as $product)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $product->SKU }}</td>
            <td>{{ $product->Name }}</td>
            <td>{{ $product->Price }}</td>
            <td>{{ $product->Category }}</td>
            <td>{{ $product->Qty }}</td>
            <td><img src="/uploads/{{ $product->id }}/{{ $product['image'] }}" height="50px" width="50px" /></td>
            <td>
                <form action="{{ route('products.destroy',$product->id) }}" method="POST">

   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $products->links() !!}
      
@endsection